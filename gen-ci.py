import argparse
import os
import subprocess

def generate_job(version, env, prod=False):
    env_upper = env.upper()
    job_name = f"deploy '{version}' to {env}"
    visibility_clause = "only:\n   - main\n" if prod else "except:\n   - main\n"

    return f"""
{job_name}:
  stage: deploy
  when: manual
  allow_failure: false
  {visibility_clause}  
  before_script:
    - node -v
    - echo "test"
  script:
    - echo "deploying to {env}"
    - node -v
    - git --version
"""

def generate_rollback_job(version, rollback_version, env):
    env_upper = env.upper()
    job_name = f"rollback '{version}' to {rollback_version} on {env}"
    return f"""
{job_name}:
  stage: deploy
  when: manual
  allow_failure: true  
  only:
    - main  
  script:
    - echo "Rolling back {env} to version {rollback_version}"
    # Add your specific rollback commands here 
"""

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate dynamic GitLab CI configuration.")
    parser.add_argument("-v", "--version", required=True, help="The version to deploy")
    parser.add_argument("-e", "--environments", required=True, help="Comma-separated list of environments")
    parser.add_argument("-r", "--rollback-version", required=False, help="Version to rollback to (for prod rollback job)")
    args = parser.parse_args()

    output = """image: silverarrow/amz-node-20:1.1.0

stages:
  - test
  - deploy
"""

    # Add nginx test job
    output += """
test nginx config:
  stage: test
  image: nginx  
  script:
    - apk add --update nginx # Example: Install nginx (if needed)
    - nginx -t -c /path/to/your/nginx.conf  
"""

    for env in args.environments.split(","):
        output += generate_job(args.version, env)
        output += "\n"

    # Only add PROD jobs if on the 'main' branch
    if os.getenv("CI_COMMIT_BRANCH") == "main": 
        output += generate_job(args.version, "PROD", prod=True)  
        output += "\n" 

        # Add rollback job if rollback version is provided
        if args.rollback_version:
            output += generate_rollback_job(args.version, args.rollback_version, "PROD")
            output += "\n"

    # Write to file
    with open("dynamic-gitlab-ci.yml", "w") as f:
        f.write(output)