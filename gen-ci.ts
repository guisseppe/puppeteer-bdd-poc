import * as fs from 'fs';
import * as os from 'os';

// Argument handling
if (process.argv.length < 4) {
  console.error("Error: Both version and environment list (comma-separated) are required.");
  process.exit(1);
}

const version = process.argv[2];
const environments = process.argv[3].split(',');

// Templates with 'script' section
const dynamicJobTemplate = `
deploy {version} to {env}:
  stage: deploy
  when: manual
  allow_failure: false
  
  before_script:
    - node -v
    - echo "test"
  
  script:
    - echo "deploying to {target}"
    - node -v
    - git --version`;

const finalJobTemplate = `
deploy {version} to PROD:
  stage: deploy-to-prod
  when: manual
  
  before_script:
    - node -v
    - echo "test"
  
  script:
    - echo "deploying to {target}"
    - node -v
    - git --version`;

// Generate output
let output = `image: silverarrow/amz-node-20:1.1.0

stages:
  - deploy
  - deploy-to-prod
`;

for (const env of environments) {
  const envUpper = env.toUpperCase();
  output += dynamicJobTemplate.replace(/{version}/g, version).replace(/{env}/g, envUpper).replace(/{target}/g, env) + os.EOL;
}

output += finalJobTemplate.replace(/{version}/g, version);

// Write to file
fs.writeFileSync('dynamic-gitlab-ci.yml', output);