## Pipeline Stages:

The pipeline consists of three main stages:

1. **test:** This job syntax checks the Nginx config

2. **deploy (for develop branch):**
    * This stage specifically when you merge the `develop` branch.
    * It includes manual deployment jobs to non-prod PCF environments.

3. **deploy-to-prod (for main branch):**
    * This stage runs when you merge to the `main` branch.
    * It contains a manual deployment job to PROD. The job requires the variable **CHANGE_RECORD** to be set with a valid change record.

## Key Points
* **Manual Approval:**  Deployment jobs in both stages are configured to require manual approval before they can run (excluding the test job which is automatic). This allows you to review the changes before deploying.
* **Placeholder Jobs:**  If you commit to a branch other than `develop` or `main`, the script inserts placeholder jobs into the `deploy` and `deploy-to-prod` stages. This ensures the pipeline completes successfully without breaking.
