Feature: Puppeteer PoC
    
    @Record
    Scenario: Natwest Moprtgages Login Test
        Given I am on "https://spa.mortgages.natwest.com/servicing/login"
        When I play the user flow recording "mss-test.json"
        And I generate a Lighthouse User Flow report from "mss-test.json"