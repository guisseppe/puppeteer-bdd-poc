#!/bin/bash

# Define flags and their defaults
version=""
environments=""

# Parse flags and arguments using getopts
while getopts ":v:e:" opt; do
 case $opt in
  v) version="$OPTARG" ;;
  e) environments="$OPTARG" ;;
  \?) echo "Invalid option: -$OPTARG" >&2; exit 1;;
 esac
done

# Shift away processed options to access positional arguments
shift $((OPTIND-1))

# Check for required arguments
if [[ -z "$version" || -z "$environments" ]]; then
 echo "Error: Both -v (version) and -e (environments, comma-separated) are required."
 exit 1
fi

# Replace commas with spaces for iteration
environments="${environments//,/ }"

# Template for deploy jobs 
deployJobTemplate='
deploy '"$version"' to {env}:
 stage: deploy
 when: manual
 allow_failure: false
 before_script:
  - node -v
  - echo "test"
 script:
  - echo "deploying to {target}"
  - node -v
  - git --version'

# Generate output
output="image: silverarrow/amz-node-20:1.1.0

stages:
 - deploy
 - deploy-to-prod
"

for env in $environments; do
 envUpper=$(echo "$env" | tr '[:lower:]' '[:upper:]')
 output+=$(sed -e "s/{version}/$version/g" \
        -e "s/{env}/$envUpper/g" \
        -e "s/{target}/$env/g" <<< "$deployJobTemplate")
 output+=$'\n'
done

# PROD job (only included on the 'main' branch)
if [[ "$CI_COMMIT_BRANCH" == "main" ]]; then
  output+=$(sed "s/{version}/$version/g" <<< "$finalJobTemplate")
  output+=$'\n'
fi

# Write to file
echo "$output" > dynamic-gitlab-ci.yml